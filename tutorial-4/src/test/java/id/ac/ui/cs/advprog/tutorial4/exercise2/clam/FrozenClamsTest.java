package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FrozenClamsTest {
    private FrozenClams froze;

    @Before
    public void setUp(){
        froze = new FrozenClams();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Frozen Clams from Chesapeake Bay", froze.toString());
    }
}
