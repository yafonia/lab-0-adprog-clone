package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThickCrustDoughTest {
    private ThickCrustDough thick;

    @Before
    public void setUp(){
        thick = new ThickCrustDough();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("ThickCrust style extra thick crust dough", thick.toString());
    }
}
