package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ReggianoCheeseTest {
    private ReggianoCheese regiano;

    @Before
    public void setUp(){
        regiano = new ReggianoCheese();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Reggiano Cheese",regiano.toString());
    }
}
