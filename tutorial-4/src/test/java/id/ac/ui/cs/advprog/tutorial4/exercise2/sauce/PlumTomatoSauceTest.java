package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PlumTomatoSauceTest {
    private PlumTomatoSauce plum;

    @Before
    public void setUp(){
        plum = new PlumTomatoSauce();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Tomato sauce with plum tomatoes", plum.toString());
    }
}
