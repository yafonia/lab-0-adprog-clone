package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class RazorClamsTest {
    private RazorClams razor;

    @Before
    public void setUp(){
        razor = new RazorClams();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Razor Clams from Alaska", razor.toString());
    }
}
