package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {
    private PizzaStore depokPizzaStore;
    private Pizza cheesePizza;
    private Pizza veggiePizza;
    private Pizza clamPizza;

    @Before
    public void setUp(){
        depokPizzaStore = new DepokPizzaStore();
        cheesePizza = depokPizzaStore.orderPizza("cheese");
        veggiePizza = depokPizzaStore.orderPizza("veggie");
        clamPizza = depokPizzaStore.orderPizza("clam");
    }

    @Test
    public void testCheesePizza(){
        assertEquals("---- Depok Style Cheese Pizza ----\n" +
                "No Crust Dough\n" +
                "Sicilian Sauce\n" +
                "Crumbled Blue Cheese\n", cheesePizza.toString());
    }

    @Test
    public void testVeggiePizza(){
        assertEquals("---- Depok Style Veggie Pizza ----\n" +
                "No Crust Dough\n" +
                "Sicilian Sauce\n" +
                "Crumbled Blue Cheese\n"+
                "Garlic, Onion, Mushrooms, Tomato\n", veggiePizza.toString());
    }

    @Test
    public void testClamPizza(){
        assertEquals("---- Depok Style Clam Pizza ----\n" +
                "No Crust Dough\n" +
                "Sicilian Sauce\n" +
                "Crumbled Blue Cheese\n"+
                "Razor Clams from Alaska\n", clamPizza.toString());
    }

}
