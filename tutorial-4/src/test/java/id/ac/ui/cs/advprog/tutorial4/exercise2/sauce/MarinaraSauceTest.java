package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class MarinaraSauceTest {
    private MarinaraSauce marinara;

    @Before
    public void setUp(){
        marinara = new MarinaraSauce();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Marinara Sauce", marinara.toString());
    }
}
