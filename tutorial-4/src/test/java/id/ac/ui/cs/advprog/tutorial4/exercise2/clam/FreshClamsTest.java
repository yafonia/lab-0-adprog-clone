package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class FreshClamsTest {
    private FreshClams fresh;

    @Before
    public void setUp(){
        fresh = new FreshClams();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Fresh Clams from Long Island Sound", fresh.toString());
    }
}
