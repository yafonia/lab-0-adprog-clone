package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;


public class BlueCheeseTest {
    private BlueCheese blue;

    @Before
    public void setUp(){
        blue = new BlueCheese();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Crumbled Blue Cheese", blue.toString());
    }

}
