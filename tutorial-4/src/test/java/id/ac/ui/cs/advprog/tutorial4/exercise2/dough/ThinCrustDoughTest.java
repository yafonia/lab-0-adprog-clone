package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class ThinCrustDoughTest {
    private ThinCrustDough thin;

    @Before
    public void setUp(){
        thin = new ThinCrustDough();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Thin Crust Dough", thin.toString());
    }
}
