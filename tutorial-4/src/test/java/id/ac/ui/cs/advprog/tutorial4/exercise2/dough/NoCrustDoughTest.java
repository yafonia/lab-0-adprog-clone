package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class NoCrustDoughTest {
    private NoCrustDough noCrust;

    @Before
    public void setUp(){
        noCrust = new NoCrustDough();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("No Crust Dough", noCrust.toString());
    }
}
