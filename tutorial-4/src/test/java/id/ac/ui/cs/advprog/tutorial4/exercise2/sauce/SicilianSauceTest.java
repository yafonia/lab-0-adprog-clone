package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class SicilianSauceTest {
    private SicilianSauce sicilian;

    @Before
    public void setUp(){
        sicilian = new SicilianSauce();
    }

    @Test
    public void testtoStringMethod(){
        assertEquals("Sicilian Sauce", sicilian.toString());
    }
}
